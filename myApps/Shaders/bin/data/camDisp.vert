#version 120
#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

uniform float time;

void main(){
  vec4 position = ftransform();
  float displacementHeight = 20.0;
  float displacementY = sin(time + (position.x/20.0)) * displacementHeight;
  vec4 modifiedPosition = position;
  modifiedPosition.y += displacementY;
  gl_Position = modifiedPosition;
  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_FrontColor = gl_Color;
}

#version 120
#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

uniform float time;
uniform sampler2DRect texture;
float radius = .5;

void main(void){
  vec2 tc = gl_TexCoord[0].xy;
  vec2 p = -1.0 + 2.0 * tc;
  float len = length(p);
  vec2 uv = tc + 2.0 * (p/len) * cos(len * 0.12 - time * 4.0);
  vec3 col = texture2DRect(texture, uv).xyz;
  gl_FragColor = vec4(col, 1.0);
}

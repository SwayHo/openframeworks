//
//  ParticleSystem.cpp
//  ParticleTest
//
//  Created by Minhwan Ho on 2020. 2. 22..
//

#include "ParticleSystem.h"

ParticleSystem::ParticleSystem(){
    
}

ParticleSystem::~ParticleSystem(){
    
}

void ParticleSystem::setup(int n, ofVec2f o){
    numOfParticles = n;
    origin = o;
//    for (int i=0; i < numOfParticles; i++){
//        Particle *particle = new Particle();
//        particle->setup(origin, ofVec2f(ofRandom(-5.0f, 5.0f), ofRandom(-5.0, 5.0f)), ofVec2f(0.0,0.1), ofColor(0,0,0), 10.0, 200);
//        particles.push_back(particle);
//    }
    
}

void ParticleSystem::addParticle(){
    Particle *particle = new Particle();
    particle->setup(origin, ofVec2f(ofRandom(-5.0f, 5.0f), ofRandom(-5.0f, 5.0f)), ofVec2f(0.0, 0.1), ofColor(0,0,0), 10.0, 300);
    particles.push_back(particle);
}

void ParticleSystem::update(){
    addParticle();
    for(auto d: particles){
        d->update();
    }
    for(auto it = particles.begin(); it != particles.end(); ++it){
        if((*it)->isAlive() == false){
            it = particles.erase(it);
        }
    }
}

void ParticleSystem::draw(){
    for(auto d: particles){
        d->draw();
    }
}

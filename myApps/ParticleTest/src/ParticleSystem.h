//
//  ParticleSystem.h
//  ParticleTest
//
//  Created by Minhwan Ho on 2020. 2. 22..
//

#ifndef ParticleSystem_h
#define ParticleSystem_h

#include "ofMain.h"
#include "Particle.h"

class ParticleSystem {
    public :
        ParticleSystem();
        ~ParticleSystem();
    
        void setup(int n, ofVec2f o);
        void update();
        void draw();
        void addParticle();
    public :
        int numOfParticles;
        ofVec2f origin;
        vector<Particle*> particles;
};
#endif /* ParticleSystem_h */

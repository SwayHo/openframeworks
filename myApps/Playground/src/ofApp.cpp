#include "ofApp.h"

// 1. just play camera
// 2. rander camera to model
// ofTriangle or ofRactangle has no texture coordinate....
// if you attatch texture to model, try using mesh.

ofVideoGrabber camera;
ofTexture camTexture;
ofImage camDispImage;
ofShader previewShader;

ofPlanePrimitive planePrimitive;

vector<ofPoint> particle_centers;
vector<ofMesh> particles;
vector<ofMesh> planets;
vector<ofMesh> eyePlans;
vector<float> rotInfos;
float width = 640;
float height = 480;
float centerW = width/2.0f;
float centerH = height/2.0f;
int planetNum = 1500;
int eyeMeshNum = 200;
float dx = 200.0f;

//--------------------------------------------------------------
void ofApp::setup(){
    previewShader.load("texture.vert", "texture.frag");
    
    camera.setDeviceID(0);
    camera.setDesiredFrameRate(30);
    camera.initGrabber(width, height);
    
    camTexture.allocate(width, height, OF_PIXELS_RGBA);
    camDispImage.allocate(width, height, OF_IMAGE_COLOR);
    planePrimitive.set(ofGetWidth(), ofGetHeight(), 50, 50, OF_PRIMITIVE_TRIANGLES);
    planePrimitive.mapTexCoords(0, 0, 640, 480);
    
    float planet_Rad = 250.0f;
    float particle_Rad = 150.0f;
    float planet_rad = 50.0f;
    float rad2 = 100.f;
    
    float rad3 = 140.0f;
    float eye_Rad = 50.0f;
    
    // setting mesh
    for (int i=0; i<planetNum; i++) {
        ofPoint center( ofRandom( -1, 1 ),
                       ofRandom( -1, 1 ),
                       ofRandom( -1, 1 ) );
        center.normalize(); //Normalize vector's length to 1
        ofPoint center2 = center * particle_Rad;
        ofPoint center3 = center * eye_Rad;
        center *= planet_Rad;
        
        particle_centers.push_back(center2);
        ofMesh particle;
        ofMesh planet;
        
        particle.setMode(OF_PRIMITIVE_TRIANGLES);
        for (int j=0; j<3; j++) {
            ofPoint point = ofPoint( ofRandom( -planet_rad, planet_rad ),
                             ofRandom( -planet_rad, planet_rad ),
                             ofRandom( -planet_rad, planet_rad ) );
            
            ofPoint point2 = ofPoint( ofRandom( -rad2, rad2 ),
                                    ofRandom( -rad2, rad2 ),
                                    ofRandom( -rad2, rad2 ) );
            
            addVertex(&particle,  point2.x,  point2.y,  point2.z, 0, 0);
            addVertex(&planet, center.x + point.x, center.y + point.y, center.z + point.z, 0, 0);
            
        }
        
        if (i < eyeMeshNum){
            ofMesh eyeMesh;
            for (int j=0; j<3; j++) {
                ofPoint point = ofPoint( ofRandom( -rad3, rad3 ),
                                        ofRandom( -rad3, rad3 ),
                                        ofRandom( -rad3, rad3 ) );
                addVertex(&eyeMesh, center3.x + point.x, center3.y + point.y, center3.z + point.z, -0.1, 0.1);
            }
            eyePlans.push_back(eyeMesh);
        }
        
        rotInfos.push_back(ofRandom(-360.0f, 360.0f));
        particles.push_back(particle);
        planets.push_back(planet);
        
    }
    ofEnableDepthTest();
//    ofEnableSmoothing();
}

void ofApp::addVertex(ofMesh* m, float x, float y, float z, float offsetX, float offsetY){
    m->addVertex(ofPoint(x, y, z));
    m->addTexCoord(camTexture.getCoordFromPercent((x + width)/(width * 2) + offsetX,(y + height)/(height * 2)) + offsetY);
}

//--------------------------------------------------------------
void ofApp::update(){
    camera.update();
    if (camera.isFrameNew()){
        ofPixels & pixels = camera.getPixels();
        camDispImage.setFromPixels(pixels);
        camTexture.loadData(pixels);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackgroundGradient( ofColor( 128 ), ofColor( 50 ) );
    ofPushMatrix();
    ofTranslate(ofGetWidth()/3, ofGetHeight()/3, 0.0);

    
    float time = ofGetElapsedTimef();    //Get time in seconds
    float angle = time * 0.2;            //Compute angle. We rotate at speed 10 degrees per second
    ofRotate( angle, 0.01, 0.01, 0.1 );
    for (int i=0; i<planetNum; i++) {


        ofTranslate(particle_centers[i].x, particle_centers[i].y, particle_centers[i].z);
        ofRotate(time * 0.2, 0.0, 0, 0.1);
        camTexture.bind();
        particles[i].draw();
        camTexture.unbind();
        
        
    }
    ofPopMatrix();
    
    // mesh 하나당 푸쉬 한번
    ofPushMatrix();
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2, 0.0);
    float time2 = ofGetElapsedTimef();
    float angle2 = time2 * 25;
    ofRotate( angle2, 0, 1, 0 );
    for (int i=0; i<planetNum; i++) {
                camTexture.bind();
                planets[i].draw();
                camTexture.unbind();
    }
    ofPopMatrix();
    
    
    ofPushMatrix();
    ofTranslate(ofGetWidth()/3, ofGetHeight()/3, 0.0);
    float time3 = ofGetElapsedTimef();
    float angle3 = time2 * 25;
    ofRotate( angle3, 0, 1, 0 );
    for (int i=0; i<eyeMeshNum; i++) {
        camTexture.bind();
        eyePlans[i].draw();
        camTexture.unbind();
    }
    ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

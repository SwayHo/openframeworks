#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    bDraw = false;
    posX = 0;
    posY = 0;
    r = 0;
    pos.clear();
    radius.clear();
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    for(int i = 0; i < pos.size(); i++){
        ofSetColor(0, 0, 255);
        ofDrawCircle(pos[i], radius[i]);
    }
    if(bDraw){
        ofSetColor(0, 255, 0);
        ofDrawCircle(posX, posY, r);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    r = ofDist(posX, posY, x, y);
    bDraw = true;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    posX = x;
    posY = y;
    bDraw = false;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    pos.push_back(ofPoint(posX, posY));
    radius.push_back(r);
    bDraw = false;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

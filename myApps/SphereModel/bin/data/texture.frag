#version 120
#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2DRect texture;

void main(){
  vec2 uv = gl_TexCoord[0].xy;
  uv = uv -.5;
  uv *= 2.;
  uv += .5;
  vec4 color = texture2DRect(texture, uv);
  gl_FragColor = color;
}

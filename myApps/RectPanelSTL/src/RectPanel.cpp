//
//  RectPanel.cpp
//  RandomRect
//
//  Created by Minhwan Ho on 2020. 2. 20..
//

#include "RectPanel.h"

RectPanel::RectPanel() {
    
}

void RectPanel::setRectPanel(float x, float y, float w, float h, int r, int g, int b, int a){
    posX = x;
    posY = y;
    width = w;
    height = h;
    red = r;
    green = g;
    blue = b;
    alpha = a;
}

void RectPanel::draw(){
    ofSetColor(red, green, blue, alpha);
    ofDrawRectangle(posX, posY, width, height);
}

RectPanel::~RectPanel(){
    
}
